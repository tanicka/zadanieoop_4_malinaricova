/********************************************************************************
** Form generated from reading UI file 'google.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GOOGLE_H
#define UI_GOOGLE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_googleClass
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLineEdit *lineEdit;
    QPushButton *pushButton;
    QListWidget *listWidget;
    QPushButton *pushButton_2;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *googleClass)
    {
        if (googleClass->objectName().isEmpty())
            googleClass->setObjectName(QStringLiteral("googleClass"));
        googleClass->resize(768, 578);
        centralWidget = new QWidget(googleClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        QFont font;
        font.setPointSize(12);
        label->setFont(font);

        verticalLayout->addWidget(label);

        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        verticalLayout->addWidget(lineEdit);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        verticalLayout->addWidget(pushButton);

        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));

        verticalLayout->addWidget(listWidget);

        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setMinimumSize(QSize(619, 0));

        verticalLayout->addWidget(pushButton_2);

        googleClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(googleClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 768, 31));
        googleClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(googleClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        googleClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(googleClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        googleClass->setStatusBar(statusBar);

        retranslateUi(googleClass);
        QObject::connect(pushButton, SIGNAL(clicked()), googleClass, SLOT(vyhladaj()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), googleClass, SLOT(otvor()));

        QMetaObject::connectSlotsByName(googleClass);
    } // setupUi

    void retranslateUi(QMainWindow *googleClass)
    {
        googleClass->setWindowTitle(QApplication::translate("googleClass", "google", 0));
        label->setText(QApplication::translate("googleClass", "Search", 0));
        pushButton->setText(QApplication::translate("googleClass", "Vyhladaj", 0));
        pushButton_2->setText(QApplication::translate("googleClass", "Otvor", 0));
    } // retranslateUi

};

namespace Ui {
    class googleClass: public Ui_googleClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GOOGLE_H
