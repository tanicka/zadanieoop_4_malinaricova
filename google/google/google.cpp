#include "google.h"

google::google(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	
}

google::~google()
{

}

void google::vyhladaj()
{
	ui.listWidget->clear();
	hladany_vyraz = ui.lineEdit->text();
	url.setUrl(QString("http://www.google.sk/search?q=")+ hladany_vyraz);
	reply = qnam.get(QNetworkRequest(url));
	connect(reply, &QNetworkReply::finished, this, &google::citaj_stranku);

}

void google::citaj_stranku()
{
	vysledok = reply->readAll();
	vysled = vysledok.split("h3 class=");
	int i;
	for (i = 1; i < vysled.length(); i++)
	{
		int re0 = vysled[i].indexOf(QRegularExpression("\"r"), 0);
		vysled[i].remove(re0, 20);
		int re1 = vysled[i].indexOf(QRegularExpression("</a>"), 0);
		vysled[i].remove(re1, vysled[i].count());
		vysled[i].remove(QRegularExpression("<b>"));
		vysled[i].remove(QRegularExpression("</b>"));
		int re2 = vysled[i].indexOf(QRegularExpression("\">"), 0);
		vysled[i].remove(QRegularExpression("\">"));
		ui.listWidget->addItem(vysled[i].mid(re2, vysled[i].count()) + ">> " + vysled[i].remove(re2, vysled[i].count()));
	}
}

void google::otvor()
{	

	QString link = ui.listWidget->currentItem()->text();
	QStringList split_link = link.split(">> ");
	link.resize(split_link.length());
	QDesktopServices::openUrl(QUrl(split_link[1]));

}