#ifndef GOOGLE_H
#define GOOGLE_H

#include <QtWidgets/QMainWindow>
#include "ui_google.h"
#include <QNetworkAccessManager>
#include <QUrl>
#include <QtNetwork>
#include <qlineedit.h>
#include <QListWidget>
#include <qstring.h>
#include <QDesktopServices>
#include <qmessagebox.h>
#include <qregexp.h>


class google : public QMainWindow
{
	Q_OBJECT

public:
	google(QWidget *parent = 0);
	~google();
	public slots:
		void vyhladaj();
		void otvor();
		void citaj_stranku();

private:
	Ui::googleClass ui;
	QUrl url;
	QNetworkAccessManager qnam;
	QNetworkReply *reply;
	QString hladany_vyraz;
	QString vysledok;
	QStringList vysled;
	QString titulok;

};

#endif // GOOGLE_H
